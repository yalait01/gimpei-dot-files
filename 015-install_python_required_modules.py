import os
import glob
from concurrent.futures import ThreadPoolExecutor as TPE
from collections import namedtuple
import re

# pipがハングアップすることがあり、こちらを参照
# https://github.com/pypa/pip/issues/7883
os.environ["PYTHON_KEYRING_BACKEND"] = "keyring.backends.null.Keyring"
UidPath = namedtuple("UidPath", ["uid", "path"])
HOME = os.environ["HOME"]
python_paths = []
python_paths += [UidPath(uid=1000, path=path) for path in glob.glob(f"{HOME}/.pyenv/versions/*")]
python_paths += [UidPath(uid=1000, path=path) for path in glob.glob(f"{HOME}/.opt/cpython*/")]
python_paths.append( UidPath(uid=0, path="/usr/bin/python3") )


def parallel_install(python_path):
    """
    pypy, minicondaはサポートしない
    """
    if "pypy" in python_path:
        return
    elif "miniconda" in python_path:
        return

    for package in [
        "loguru",
        "mecab-python3",
        "pip",
        "bs4",
        "requests",
        "pandas",
        "neovim",
        "wget",
        "autopep8",
        "inquirer",
        "jedi",
        "jupyter",
        "matplotlib",
        "html5",
        "lxml",
        "colored",
        "dbxfs",
        "black",
        "distro",
        "tqdm",
        "selenium",
        "psutil",
        "httpx",
        "fake_useragent",
        "aiohttp_socks",
    ]:
        if re.search("^/usr/bin/python3", python_path.path):
            path = python_path.path
        else:
            path = python_path.path + "/bin/python3"

        if python_path.uid in {0}:
            os.system(f"sudo {path} -m pip install {package} --upgrade")
        elif python_path.uid in {1000}: 
            os.system(f"{path} -m pip install {package} --upgrade")

with TPE(max_workers=64) as exe:
    exe.map(parallel_install, python_paths)
