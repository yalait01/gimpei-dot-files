import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import os
import re
import shutil
from pathlib import Path
from utils import cripter
HOME = os.environ["HOME"]
sysname = os.uname().sysname

url = "https://github.com/dropbox/dbxcli/releases"
mst_p = urlparse(url)
mst_netloc = mst_p.netloc
mst_scheme = mst_p.scheme
r = requests.get(url)
soup = BeautifulSoup(r.text, features="html5lib")


hrefs = []
for a in soup.find_all("a", {"href": True}):
    p = urlparse(a.get("href"))

    if p.netloc == "":
        p = p._replace(scheme=mst_scheme, netloc=mst_netloc)

    href = p.geturl().strip()
    if sysname == "Linux":
        if re.search("^.{1,}/dbxcli-linux-amd64$", href):
            hrefs.append(href)
    elif sysname == "Darwin":
        if re.search("^.{1,}/dbxcli-darwin-amd64$", href):
            hrefs.append(href)
    else:
        raise Exception("This os is not supported.")

# 先頭のものが最新のはず
url = hrefs.pop(0)
r = requests.get(url)
binary = r.content

with open(f"{HOME}/.bin/dbxcli", "wb") as fp:
    fp.write(binary)

Path(f"{HOME}/.bin/dbxcli").chmod(0o555)

cipher = b'gAAAAABelpLdtZDqVOgXDsCevYpKqfysM8P0hi2yRsfEtGjcqXyzNE4JbVwxHy9Zahr31p79BlThBMR6g69qmAAU-S9q0PzQtIYku1EfrUWjTMN2g9JODDwz35_SwXnVA1VMrH1pbCUuSY0wxt_dWMSXc9yPtexYkUQf48r1tnMUv1WICJzNSZWyBFBvNxcG9Fy4uOr_s1Vy'
key = input('please input dbxcli"s credential password.')
credential = cripter.decription(key=key, ciphertext=cipher).decode()

Path(f'{HOME}/.config/dbxcli/').mkdir(exist_ok=True, parents=True)
with open(f'{HOME}/.config/dbxcli/auth.json', 'w') as fp:
    fp.write(credential)
