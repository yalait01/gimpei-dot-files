import os
from pathlib import Path
import re
import requests
from bs4 import BeautifulSoup
import zipfile
import distro

distro_name, version, codename = distro.linux_distribution(full_distribution_name=False)

# もし、Ubunutu, Debianであったらdebを入れる
if distro_name.lower() in {"ubuntu", "debian", "elementary"}:
    # this program is too buggy, so another install way is desired.
    if not Path("/tmp/google-chrome-stable_current_amd64.deb").exists():
        os.system(
            "wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -O /tmp/google-chrome-stable_current_amd64.deb"
        )
    os.system("sudo apt install /tmp/google-chrome-stable_current_amd64.deb")

r = requests.get(f"https://chromedriver.chromium.org/downloads")
soup = BeautifulSoup(r.text, features="lxml")

HOME = os.environ["HOME"]
urls = []
for a in soup.find_all("a", {"href": True}):
    match = re.search(
        "(https://chromedriver.storage.googleapis.com/\d{1,}\.\d{1,}\.\d{1,}\.\d{1,}/)",
        a.get("href"),
    )
    if match is None:
        continue

    # debina, ubuntuならlinux64で
    if distro_name.lower() in {"ubuntu", "debian", "elementary"}:
        target_url = match.group(1) + "chromedriver_linux64.zip"
    elif distro_name.lower() in {"darwin"}:
        target_url = match.group(1) + "chromedriver_mac64.zip"
    match = re.search(
        "https://chromedriver.storage.googleapis.com/(\d{1,}\.\d{1,}\.\d{1,}\.\d{1,})/",
        a.get("href"),
    )
    version = match.group(1)
    print("download", target_url, "version", version)

    with open(f"{HOME}/.tmp/chromedriver_{version}.zip", "wb") as fp:
        r = requests.get(target_url)
        fp.write(r.content)

    zfp = zipfile.ZipFile(f"{HOME}/.tmp/chromedriver_{version}.zip")
    # print(zfp.namelist())
    for name in zfp.namelist():
        zfp.extract(name, path="/tmp")
        path = Path(f"/tmp/{name}")
        path.rename(f"{HOME}/.bin/{name}_{version}")
        path = Path(f"{HOME}/.bin/{name}_{version}")
        path.chmod(0o555)
