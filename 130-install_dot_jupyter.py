from pathlib import Path
import os
import sys
import shutil

PWD = Path(__file__).resolve().parent
HOME = os.environ["HOME"]

if not Path(f"{HOME}/.jupyter").exists():
    print(".jupyter folder is not exist, so try to create folder.")
    Path(f"{HOME}/.jupyter").mkdir(exist_ok=True)

shutil.copy(f"{PWD}/files/jupyter_notebook_config.py", f"{HOME}/.jupyter")
