import os
import sys
from pathlib import Path
import AddBashrc

HOME = os.environ["HOME"]

# ~/.binを作成
Path(f"{HOME}/.bin").mkdir(exist_ok=True, parents=True)

ctx = "export PATH={HOME}/bin:$PATH"
AddBashrc.AddBashrc(ctx)

# ~/.tmpを作成
Path(f"{HOME}/.tmp").mkdir(exist_ok=True, parents=True)

# ~/.varを作成
Path(f"{HOME}/.var").mkdir(exist_ok=True, parents=True)

# ~/.optを作成
Path(f"{HOME}/.opt").mkdir(exist_ok=True, parents=True)

# ~/.mntを作成
Path(f"{HOME}/.mnt").mkdir(exist_ok=True, parents=True)

# これはプロダクトに対するオプションであるが自動化しておくと非常に楽なので実装する
# for i in range(20):
#    Path(f"{HOME}/.mnt/favs{i:02d}").mkdir(exist_ok=True, parents=True)

# これはサーバのマッピング状況である
for svr in [12, 14, 16, 19, 20, 21, 22, 23]:
    Path(f"{HOME}/.mnt/{svr:02d}").mkdir(exist_ok=True, parents=True)

