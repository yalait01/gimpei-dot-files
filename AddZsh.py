import os
from pathlib import Path

HOME = os.environ["HOME"]
Path(f"")
path_zshrc = f"{HOME}/.zshrc"


def AddBashrc(ctx):
    with open(path_bashrc) as fp:
        bashrc = fp.read()

    if ctx in bashrc:
        return "Already appended to zshrc"

    bashrc = bashrc + "\n" + ctx

    with open(path_bashrc, "w") as fp:
        fp.write(bashrc)
