import os
from pathlib import Path
import shutil

HOME = os.environ["HOME"]
shutil.copy("files/ssh_config", f"{HOME}/.ssh/config")
shutil.copy("files/authorized_keys", f"{HOME}/.ssh/authorized_keys")
shutil.copy("files/id_github.pub", f"{HOME}/.ssh/id_github.pub")
