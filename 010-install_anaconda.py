import os
import sys
import requests
from bs4 import BeautifulSoup
import AddBashrc
from pathlib import Path
import re
import shutil

HOME = os.environ["HOME"]
download_url = "https://www.anaconda.com/distribution/"

r = requests.get(download_url)
soup = BeautifulSoup(r.text)

hrefs = []
for e in soup.find_all("a", {"href": True}):
    href = e.get("href")
    print(href)
    if re.search(
        r"^https://repo.anaconda.com/archive/Anaconda3-.*?-Linux-x86_64.sh$", href
    ):
        hrefs.append(href)

print("there are all versions of neovim...")
for link in hrefs:
    print(link)
print("try to donload head versions of list.")

# binのpathは005で追加されている
# -b : batch mode -> bashrcを編集しない
#
os.system(f"wget {hrefs[0]} -O {HOME}/.var/anaconda.sh")
os.system(f"bash {HOME}/.var/anaconda.sh -b -p {HOME}/.opt/anaconda")

"""
~/.zshrcにPATHを追加
"""

with open(f"{HOME}/.zshrc", "a") as fp:
    fp.write("export PATH=$HOME/.opt/anaconda/bin:$PATH\n")
print("PATHを追加したのでsource ~/.zshrcしてください")
print("Ex) conda install -c anaconda python=3.8")
print("Ex) conda update --all")
