#!/usr/bin/python3

import os
import sys
from subprocess import Popen
from subprocess import PIPE
import re
import json
import io
from pathlib import Path
from collections import namedtuple
from pygments import highlight, lexers, formatters

def coloring(x):
    colorful_json = highlight(bytes(x, 'UTF-8'), 
                        lexers.JsonLexer(), 
                        formatters.Terminal256Formatter(style='fruity'))
    return colorful_json

def get_smart(device='sda'):
    path = f'/dev/{device}'
    with Popen(['sudo', 'smartctl', '-a', path, '--json'], stdin=PIPE, stdout=PIPE) as proc:
        stdout, stderr = proc.communicate()
        # print(stdout.decode().strip())
        ret = json.loads(stdout.decode('utf8', 'ignore').strip())

        model_family = ret.get('model_family')
        model_name = ret.get('model_name')
        serial_number = ret.get('serial_number')

        if 'nvme' in device:
            nvme_smart_health_information_log = ret['nvme_smart_health_information_log']
            out =  {'model_family':model_family, 
                    'model_name':model_name, 
                    'serial_number':serial_number,
                    'nvme_smart_health_information_log':nvme_smart_health_information_log
                    }
            return out
        
        else:
            interface_speed = ret.get('interface_speed')
            # トータル書き込み量
            try:
                total_flush = [r for r in ret['ata_smart_attributes']['table'] if r['id'] in set([241, 245])].pop()
                name = total_flush['name']
                value = total_flush['flags']['value']
                total_flush = {name:value}
            except Exception as exc:
                print(exc)
                total_flush = None
            # 温度
            try:
                temperture = [r for r in ret['ata_smart_attributes']['table'] if r['id'] in set([194])].pop()
                temperture = temperture['flags']['value']
            except:
                temperture = None
            out =  {'model_family':model_family, 
                    'model_name':model_name, 
                    'serial_number':serial_number,
                    'interface_speed':interface_speed,
                    'temperture':temperture,
                    'total_flush':total_flush}
            return out

Datum = namedtuple('Datum', ['fstype', 'mount_point', 'fsavail', 'smart'])
def main():
    with Popen(['lsblk', '-f', '--json'], stdin=PIPE, stdout=PIPE) as proc:
        name_datum = {}
        proc.wait(8)
        reader = io.TextIOWrapper(proc.stdout)
        obj = json.loads(reader.read())
        values = list(obj.values())
        values = values[0] # need unpack
        for value in values:
            p_name = value['name']
            fstype = value['fstype']
            mount_point = value['mountpoint']
            # '/snap/hogehoge' のとき、スキップする
            if mount_point and re.search('^/snap', mount_point):
                continue
            smart = get_smart(device=p_name)
            fsavail = value['fsavail']
            name_datum[p_name] = Datum(fstype=fstype, 
                                    mount_point=mount_point, 
                                    fsavail=fsavail, 
                                    smart=smart)

    args = sys.argv[1:]
    # 最後がデバイスpath
    device_path = sys.argv[-1]
    if not Path(device_path).exists():
        print('specified device is not exists.')
        exit(1)
    if len(sys.argv) <= 1:
        outs = []
        for name, datum in name_datum.items():
            device_name = name
            with open(f'/sys/block/{device_name}/queue/rotational') as fp:
                disk_type = fp.read().strip()[-1] # get last char
                disk_type = 'SSD' if disk_type == '0' else 'HDD'
            out = {
                    'device_name':device_name, 
                    'datum':datum._asdict(),
                    'disk_type':disk_type}
            outs.append(out)
        print(coloring(json.dumps(outs, indent=2)).strip())

    else:
        device_name = Path(device_path).name
        datum = name_datum[device_name]
        #print('device name',device_name, formats)
        # cat /sys/block/{device_name}/queue/rotational
        # これの最後が0ならSSD、1ならHDD
        # referer: https://unix.stackexchange.com/questions/65595/how-to-know-if-a-disk-is-an-ssd-or-an-hdd
        with open(f'/sys/block/{device_name}/queue/rotational') as fp:
            disk_type = fp.read().strip()[-1] # get last char
            disk_type = 'SSD' if disk_type == '0' else 'HDD'
        
        out = {
                'device_name':device_name, 
                'datum':datum._asdict(),
                'disk_type':disk_type}
        print(coloring(json.dumps(out, indent=2)))

            
if __name__ == '__main__':
    main()
