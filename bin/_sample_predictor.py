import os
import sys
import random

"""
 expected input : [a-z]
 expected output : [a-z]
 e.g. : a -> b, b -> c, c -> d, ...
 同期で入力を受け取って推論を帰すpredictor
 process立ち上げコストが大きいと仮定すると使いまわしたほうがいい
"""


def load_model():
    # めちゃ重いlaod処理だとしたら
    m = 0
    for i in range(10 ** 7):
        m += random.random()
    m /= 10 ** 7
    kv = {
        k: v for k, v in zip("abcdefghijklmnopqrstuvwxyz", "bcdefghijklmnopqrstuvwxyza")
    }
    return kv


kv = load_model()


def predictor(inputs):
    if inputs == "E":
        exit(0)
    ret = kv.get(inputs)
    if ret is None:
        return "E"
    else:
        return ret


for ch in sys.stdin:
    ch = ch.strip()
    sys.stdout.write(predictor(ch) + "\n")
    sys.stdout.flush()
