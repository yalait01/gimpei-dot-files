import os
import shutil
import distro

distro_name, version, codename = distro.linux_distribution(full_distribution_name=False)

if 'ubuntu' in distro_name.lower():
    os.system('sudo apt-get install libusb-1.0-0 libusb-1.0-0-dev')
    os.system('git clone https://github.com/mkorenkov/ipad_charge; cd ipad_charge; make; sudo make install')
    shutil.rmtree("ipad_charge")
else:
    raise Exception("not supported os!")

