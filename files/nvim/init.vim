set number
set notermguicolors
set mouse-=a
set clipboard=unnamed
set tabstop=4
set shiftwidth=2
set encoding=utf-8
set termencoding=utf-8
set fenc=utf-8
set sh=zsh
set syntax=on

function CreateState() 
  echo "create state start..."
  if getline(1) =~ 'standardSQL'
    colorscheme darkblue
  else
    colorscheme monokai
  endif

  echo "setted a colorschme..."
  :w
  echo "overiwrite a file"
  :UpdateRemotePlugins
  echo "update remote"
  
  call deoplete#enable()
  echo "force enable deoplete"
  :UpdateRemotePlugins
  set shiftwidth=4
  set tabstop=4
  set expandtab
  " pythonファイルならば以下の処理を
  " 実行
  if @% =~# '.py'
    ":call Autopep8()
    " Blackを読んでからAutopep8にする
    let g:black_linelength=120
    let g:black_skip_string_normalization=1
    :Black
   
    " Autopep8のドキュメントであるここを読む必要がある
    " https://github.com/tell-k/vim-autopep8
    let g:autopep8_max_line_length=320
    let g:autopep8_disable_show_diff=1
    :Autopep8
    
    " tab -> space
    if search('\t')
      :%s/\t/    /g
    endif
    
    " 色の修正
    highlight Folded ctermbg=7 ctermfg=0
  endif
endfunction

function ApplyPep8()
  :Autopep8
endfunction

function ApplyEmoji()
    :%s/:\([^:]\+\):/\=emoji#for(submatch(1), submatch(0))/g
endfunction

function ChangeBackground() 
  colorscheme darkblue
endfunction

" foldingのフォーマットがイケてないので編集する
function! MyFoldText()
  let nblines = v:foldend - v:foldstart + 1
  let w = winwidth(0) - &foldcolumn - (&number ? 8 : 0)
  let line = getline(v:foldstart)
  let comment = substitute(line, '/\*\|\*/\|{{{\d\=', '', 'g')
  let expansionString = repeat(".", w - strwidth(nblines.comment.'"'))
  let txt = comment . "@folding@" . nblines
  return txt
endfunction
set foldtext=MyFoldText()
set foldcolumn=1

" 作成したfoldingを保存
augroup remember_folds
  try
    autocmd!
    autocmd BufWinLeave ?* silent! mkview
    autocmd BufWinEnter ?* silent! loadview
  catch
    echo "missed mkview"
  endtry
augroup END


function Terminal()
  :terminal
  " set nonu
  :set nonu
  " insertmodeにしないとすぐに使えない
  :startinsert
endfunction



" define leader
let mapleader = "\<Space>"


" map keys
nmap <C-f><C-f> :<C-u>Denite file_rec -highlight-mode-insert=Search<CR>
" nmap <C-s><C-s> :<C-u>call CreateState()<CR>
nmap <C-s><C-p> :<C-u>:!autopep8 -i "%"<CR>
nmap <C-s><C-x> :<C-u>call ChangeBackground()<CR>
nmap <C-s><C-t> :<C-u>call Tab2Space()<CR>
nmap <C-x><C-x> :<C-u>call Terminal()
nmap <Alt-Up> <PageUp>
nmap <Alt-Down> <PageDown>
nmap <C-Up> <PageUp>
nmap <C-Down> <PageDown>

" enable vim-airlines
let g:airline#extensions#tabline#enabled = 1

" split vertiacal
nmap <Leader>v :vs<CR>
" split horizontal
nmap <Leader>h :sp<CR>
nmap <Leader><Up> <C-w>k
nmap <Leader><Down> <C-w>j
nmap <Leader><Left> <C-w>h
nmap <Leader><Right> <C-w>l

" launch deol 
nmap <Leader>d :Deol -split=floating<CR>

" 絵文字コードがある時、絵文字を適応
nmap <Leader>e :<C-u>call ApplyEmoji()<CR>

" save and reformat file
nmap <Leader>v :<C-u>call CreateState()<CR>
" option + r => call registers(clipboard buffers)
nmap <Leader>r :registers 
" open terminal
nmap <Leader>t :call Terminal()<CR>
" quit Leader + w
nmap <Leader>q :q<CR>
" save Leader + w
nmap <Leader>w :w<CR>
" change visual line mode
nmap <Leader><Leader> V
" map copy and paste with system clipboard
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P

nmap ]` `]
nmap [` `[



let g:python3_host_prog = expand('/usr/bin/python3')
let s:dein_dir = expand('~/.cache/dein')
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

let g:python2_host_prog = expand('/usr/bin/python')


if &runtimepath !~# '/dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . fnamemodify(s:dein_repo_dir, ':p')
endif

if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)

  let g:dir       = expand('~/.config/dein')
  let s:toml      = g:dir . '/realtime.toml'
  let s:lazy      = g:dir . '/lazy.toml'

  call dein#load_toml(s:toml,      {'lazy': 0})
  call dein#load_toml(s:lazy,      {'lazy': 1})
  call dein#add('nvie/vim-flake8')
  call dein#add('tell-k/vim-autopep8')
  " deoplete-jediはpandasでエラーが発生するので外す
  " call dein#add('deoplete-plugins/deoplete-jedi')
  call dein#add('deoplete-plugins/deoplete-dictionary')
  " call dein#add('dense-analysis/ale')
  colorscheme monokai
  " call dein#save_state()
  " これを入れておかないと、適切に最初にdeinとpynvimサーバが疎通しない
  " ref. https://github.com/Shougo/dein.vim/blob/master/doc/dein.txt
  call dein#remote_plugins()
endif

if dein#check_install()
  call dein#install()
endif

" enable deoplete
let g:deoplete#enable_at_startup = 1

call plug#begin('~/.vim/plugged')

  " Plug 'junegunn/vim-emoji'
  " Plug 'fszymanski/deoplete-emoji'
  " なにか自分で開発する際に必要
  " Plug 'jacobsimpson/nvim-example-python-plugin'
  " Plug 'Yilin-Yang/vim-markbar'
  Plug 'kshenoy/vim-signature'
  Plug 'godlygeek/tabular'
  Plug 'plasticboy/vim-markdown'
  " airline
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  " treesitter
  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
  " telescope
  Plug 'nvim-lua/popup.nvim'
  Plug 'nvim-lua/plenary.nvim'
  Plug 'nvim-telescope/telescope.nvim'
  Plug 'nvim-telescope/telescope-symbols.nvim'
  " ale
  Plug 'dense-analysis/ale'
  " jedi
  Plug 'davidhalter/jedi-vim'

call plug#end()


" nvim-treesitter
lua <<EOF
require'nvim-treesitter.configs'.setup {
  ensure_installed = "maintained", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  highlight = {
    enable = true,              -- false will disable the whole extension
    disable = { "vue", "ruby" },  -- list of language that will be disabled
  },
}
EOF

" jedi
let g:jedi#goto_stubs_command = "<leader>c"
let g:jedi#use_splits_not_buffers = "left"
let g:jedi#popup_on_dot = 0
let g:jedi#show_call_signatures = "0"




" tablineは使わない
let g:airline#extensions#tabline#enabled=0

" foldmethodをmanualに(pluginが変な影響を及ぼす)
set foldmethod=manual
let g:vim_markdown_folding_disabled=1
" 見えなくなる文字がありみずらい
let g:vim_markdown_conceal = 0
let g:vim_markdown_conceal_code_blocks = 0

" foldedのカラーフォーマットを編集
highlight Folded ctermbg=7 ctermfg=0
highlight FoldColumn ctermbg=black ctermfg=222 cterm=bold

"
function! Convert()
py3 << EOF
import vim
import re
bracketed = re.compile("{.*?}")
def convert():
    line = vim.current.line
    #vim.current.line = ret
    print(line)
convert()
EOF
endfunction


