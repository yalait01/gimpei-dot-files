import os
import sys
import yaml
from pathlib import Path

obj = yaml.load(open("./files/01-network-manager-all.yaml"), Loader=yaml.FullLoader)
print("loaded template yaml", obj)

print("input ip address with netmask.")
print("original is", obj["network"]["ethernets"]["eno1"]["addresses"])

obj["network"]["ethernets"]["eno1"]["addresses"] = [input()]

name = Path("./files/01-network-manager-all.yaml").name
with open(f"/tmp/{name}", "w") as fp:
    yaml.dump(obj, fp)

# 最後に /tmp/01-network-manager-all.yaml -> /etc/netplan/01-network-manager-all.yaml にコピーして
# sudo netplan apply すればOK(テスト済み)
