import requests
from bs4 import BeautifulSoup
import re
import os
from pathlib import Path
import shutil 

# すでにhamachiがインストール済みならば、アンインストール
if shutil.which("hamachi"):
    os.system('sudo apt remove "hamachi*" -y')
    

r = requests.get("https://www.vpn.net/linux")
soup = BeautifulSoup(r.text)

hrefs = []
for e in soup.find_all("a", {"href": True}):
    href = e.get("href")
    if re.search(r"amd64.deb$", href):
        print(e.get("href"))
        hrefs.append(e.get("href"))

os.system(f"wget https://www.vpn.net{hrefs[0]} -O hamachi.deb")

os.system("sudo apt install ./hamachi.deb")

os.system("sudo hamachi start")

# this command need, to enable hamachi
os.system("sudo hamachi login")

os.system("sudo hamachi do-join 414-476-549")

Path("hamachi.deb").unlink()
