import sys
from pathlib import Path
from os import environ as E
import os

HOME = E.get("HOME")

"""
現在のカラーセットを確認
"""
with open(f'{HOME}/.tmux.conf', 'r') as fp:
    color_line = [line.strip() for line in fp if "set-option -g status-bg" in line][0]
print('original color definition is', color_line)

"""
git repositoryを最新化
"""
os.system("git pull")

"""
新しいデータを読み込む
"""
with open('files/tmux.conf') as fp:
    conf_lines = [line.strip() for line in fp if "set-option -g status-bg" not in line]
conf_lines.insert(0, color_line)

"""
updateして保存
"""
with open(f'{HOME}/.tmux.conf', 'w') as fp:
    fp.write('\n'.join(conf_lines))

print('update all .tmux.conf!')
