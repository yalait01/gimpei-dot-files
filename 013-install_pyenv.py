import os
import sys
import AddBashrc
from pathlib import Path
import re
from concurrent.futures import ProcessPoolExecutor as PPE
import distro

distro_name, version, codename = distro.linux_distribution(full_distribution_name=False)

HOME = os.environ["HOME"]
if not Path(f"{HOME}/.pyenv").exists():
    """
    deploy ~/.pyenv
    """
    os.system("curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash")
else:
    print("~/.pyenv folder is already exists. so, try to update .pyenv git folder.")
    os.system(f'/bin/bash -c "cd {HOME}/.pyenv && git pull"')
# 実行PATHを追加
ctx = f'''
export PATH="{HOME}/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"'''

result = AddBashrc.AddBashrc(ctx)
print(result)

if distro_name.lower() in {"ubuntu"}:
    # コンパイルを追加
    dependencies = """make build-essential libssl-dev zlib1g-dev libbz2-dev \
    libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
    xz-utils tk-dev libffi-dev liblzma-dev python-openssl git"""
    os.system(f"sudo apt install {dependencies}")

elif distro_name.lower() in {"darwin"}:
    os.system("brew install pyenv")
    os.system("brew install pyenv-virtualenv")

# parse versions
versions = [v.strip() for v in os.popen('pyenv install --list').read().strip().split()]
# 一定以上のpythonを入れる
installs = []
for ver in versions:
    ver = ver.strip()
    if re.search(r"^3.[7|8|9].\d{1,}$", ver):
        print(ver)
        installs.append(ver)
    # elif re.search(r"^pypy3.[6|7|8|9]-\d.\d.\d$", ver):
    #    print(ver)
    #    installs.append(ver)
    #elif re.search(r"^miniconda3-[4|5|6|7|8|9].\d{1,}.\d{1,}$", ver):
    #    print(ver)
    #    installs.append(ver)


def parallel(arg):
    ver = arg
    # すでにインストール済みの場合、繰り返し入れない
    os.system(f'echo "N" | pyenv install {arg}')


with PPE(max_workers=64) as exe:
    exe.map(parallel, installs, timeout=60 * 7)
