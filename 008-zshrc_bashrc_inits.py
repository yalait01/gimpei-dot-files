import AddBashrc
import AddZsh
from pathlib import Path
import os
import shutil
import datetime

now = datetime.datetime.now()
now_name = now.strftime("%Y-%m-%d %H:%M:%S")
HOME = os.environ["HOME"]
if Path(f"{HOME}/.bashrc").exists():
    Path(f"{HOME}/.bashrc").rename(f"{HOME}/.var/.bashrc_{now_name}")
shutil.copy("files/bashrc", f"{HOME}/.bashrc")
if Path(f"{HOME}/.zshrc").exists():
    Path(f"{HOME}/.zshrc").rename(f"{HOME}/.var/.zshrc_{now_name}")
shutil.copy("files/zshrc", f"{HOME}/.zshrc")
