import re
import inquirer
from pathlib import Path
import sys
import os
import shutil
import unicodedata
from colored import fg, bg, attr


def remove_escaped_charactors(x):
    return re.sub(r"\x1b.*?m", "", x)


HOME = os.environ["HOME"]

# build colored numbers
colors = []
for i in range(0, 256):
    colors.append(f'{fg(15)}{bg(i)} No. {i:03d}{attr("reset")}')

shutil.copy("files/tmux.conf", f"{HOME}/.tmux.conf")

questions = [
    inquirer.List(
        "colour",
        message="What colour do you want? ref, https://gink03.github.io/tmux/",
        choices=colors,
    ),
]
answers = inquirer.prompt(questions)
colour = remove_escaped_charactors(answers["colour"])
colour_code = re.search(r"(\d{1,})", colour).group(1)

with open(f"{HOME}/.tmux.conf") as fp:
    tmux_conf = fp.read()

# update color
tmux_conf = re.sub(r'"colour\d{1,}"', f'"colour{colour_code}"', tmux_conf)
print("new .tmux.conf", tmux_conf)

with open(f"{HOME}/.tmux.conf", "w") as fp:
    fp.write(tmux_conf)
