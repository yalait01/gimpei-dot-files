import os
from pathlib import Path

HOME = os.environ["HOME"]
path_bashrc = f"{HOME}/.bashrc"
if not Path(path_bashrc).exists():
    Path(path_bashrc).touch()


def AddBashrc(ctx):
    with open(path_bashrc) as fp:
        bashrc = fp.read()

    if ctx in bashrc:
        return "Already appended to bashrc"

    bashrc = bashrc + "\n" + ctx

    with open(path_bashrc, "w") as fp:
        fp.write(bashrc)
