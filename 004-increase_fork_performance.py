import os
import sys

try:
    os.system('bash -c "pip install distro"')
    import distro
except Exception as exc:
    print(exc)
    sys.exit(1)

distro_name, version, codename = distro.linux_distribution(full_distribution_name=False)
if distro_name.lower() in {"ubuntu", "debian", "elementary"}:
    cmd = "echo 80000 | sudo tee /proc/sys/kernel/pid_max"
    os.system(cmd)

    cmd = (
        "echo 70000 | sudo tee /sys/fs/cgroup/pids/user.slice/user-1000.slice/pids.max"
    )
    os.system(cmd)

    cmd = "ulimit -a"
    os.system(cmd)
else:
    print("サポート外のOSです")
