import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import os
import re
import shutil
from pathlib import Path

HOME = os.environ["HOME"]
sysname = os.uname().sysname

url = "https://github.com/tmux/tmux/releases"
mst_p = urlparse(url)
mst_netloc = mst_p.netloc
mst_scheme = mst_p.scheme
r = requests.get(url)
soup = BeautifulSoup(r.text, features="html5lib")


hrefs = []
for a in soup.find_all("a", {"href": True}):
    p = urlparse(a.get("href"))

    if p.netloc == "":
        p = p._replace(scheme=mst_scheme, netloc=mst_netloc)

    href = p.geturl().strip()
    if sysname == "Linux":
        if re.search("^.{1,}/.{1,}.AppImage$", href):
            hrefs.append(href)
    else:
        raise Exception("This os is not supported.")

# 先頭のものが最新のはず
url = hrefs.pop(0)
r = requests.get(url)
binary = r.content

with open(f"{HOME}/.bin/tmux", "wb") as fp:
    fp.write(binary)

Path(f"{HOME}/.bin/tmux").chmod(0o555)
