import os
import shutil
from pathlib import Path
import glob

HOME = os.environ["HOME"]
for src in glob.glob("./bin/*"):
    name = Path(src).name
    shutil.copy(src, f"{HOME}/.bin/{name}")
