import os
from pathlib import Path

HOME = os.environ["HOME"]
path_authorized_keys = f"{HOME}/.ssh/authorized_keys"


def Add(key):
    if key == "":
        return False

    if Path(path_authorized_keys).exists():
        with open(path_authorized_keys) as fp:
            authorized_keys = fp.read()
    else:
        authorized_keys = ""

    if key in authorized_keys:
        print("Already appended to bashrc", key)
        return False

    with open(path_authorized_keys, "a") as fp:
        fp.write("\n" + key)


keys = open("files/authorized_keys").read().split("\n")
for key in keys:
    Add(key)
