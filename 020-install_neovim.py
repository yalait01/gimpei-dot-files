import os
import sys
import requests
from bs4 import BeautifulSoup
import AddBashrc
from pathlib import Path
import re
import shutil
from sys import platform

HOME = os.environ["HOME"]

if "linux" in platform:
    download_url = "https://github.com/neovim/neovim/releases"

    r = requests.get(download_url)
    soup = BeautifulSoup(r.text)

    hrefs = []
    for e in soup.find_all("a", {"href": True}):
        href = e.get("href")
        if re.search(r"/neovim/neovim/releases/download/v.*?/nvim.appimage$", href):
            hrefs.append(href)

    print("there are all versions of neovim...")
    for link in hrefs:
        print(link)
    print("try to donload head versions of list.")

    # binのpathは005で追加されている
    os.system(f"wget https://github.com{hrefs[0]} -O nvim")
    Path("nvim").chmod(0o771)
    shutil.copy("nvim", f"{HOME}/.bin/nvim")
    Path("nvim").unlink()
elif platform == "darwin":
    print("In MacOS, try to install neovim via brew.")
    status = os.system(f"brew upgrade neovim")

    print("Make symbolic link to /usr/local/bin/nvim to $HOME/.bin/nvim")
    status = os.system("ln -s /usr/local/bin/nvim $HOME/.bin/nvim")
    # print(status)

# vim-plugを手動インストール
os.system("""sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'""")

# filesのdein, nvimを上書きコピーする
#  - 1. unlink ~/.config/nvim, ~/.config/dein
Path(f"{HOME}/.config").mkdir(exist_ok=True)
if Path(f"{HOME}/.config/nvim").exists():
    shutil.rmtree(f"{HOME}/.config/nvim")
if Path(f"{HOME}/.config/dein").exists():
    shutil.rmtree(f"{HOME}/.config/dein")

shutil.copytree("files/nvim", f"{HOME}/.config/nvim")
shutil.copytree("files/dein", f"{HOME}/.config/dein")

# reqirementsを入れる
os.system("python3 -m pip install neovim --upgrade")
os.system("python3 -m pip install git+git://github.com/davidhalter/jedi.git --upgrade")
# os.system("python3 -m pip install git+git://github.com/deoplete-plugins/deoplete-jedi --upgrade")
os.system("python3 -m pip install git+git://github.com/psf/black@ce14fa8b497bae2b50ec48b3bd7022573a59cdb1 --upgrade")

# macやrootならPathが異なるのでpythonのPathを書き換え
HOME = os.environ["HOME"]
with open(f"{HOME}/.config/nvim/init.vim") as fp:
    init_vim = fp.read()
# /home/gimpeiを f{HOME}に書き換え
init_vim = init_vim.replace("/home/gimpei", f"{HOME}")
with open(f"{HOME}/.config/nvim/init.vim", "w") as fp:
    fp.write(init_vim)
