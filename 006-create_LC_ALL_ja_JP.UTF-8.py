import os


def main():
    """
    `/bin/bash: warning: setlocale: LC_ALL: cannot change locale (ja_JP.UTF-8)`のようなエラーを解消する。
    utf-8がシステムのデフォルトに設定されていないと、lsコマンドでutf-8が表示されないなどの問題が発生する
    """
    os.system("sudo localedef -f UTF-8 -i ja_JP ja_JP.UTF-8")

if __name__ == "__main__":
    main()
