
import os
import sys
import requests
from bs4 import BeautifulSoup
import AddBashrc
from pathlib import Path
import re
import shutil
from concurrent.futures import ThreadPoolExecutor
import distro
import lxml

HOME = os.environ["HOME"]

distro_name, version, codename = distro.linux_distribution(full_distribution_name=False)

if distro_name.lower() in {"ubuntu"}:
    # コンパイルを追加
    dependencies = """make build-essential libssl-dev zlib1g-dev libbz2-dev \
                    libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
                    xz-utils tk-dev libffi-dev liblzma-dev git"""
    os.system(f"sudo apt install {dependencies}")



download_url = "https://github.com/python/cpython/releases"

hrefs = []
for i in range(10):
    with requests.get(download_url) as r:
        soup = BeautifulSoup(r.text, "lxml")
    for e in soup.find_all("a", {"href": True}):
        href = e.get("href")
        print(href)
        if re.search(
            r"^/python/cpython/archive/refs/tags/v3.\d{1,}.\d{1,}.zip$", href
        ):
            hrefs.append(href)
        """
        ページ送りする
        """
        if e.text.strip() == "Next":
            download_url = e.get("href")
        
for href in hrefs:
    print("候補", href)

"""
ビルドとインストール
"""
# for href in hrefs:

def build_and_install(href):
    name = href.split("/")[-1]
    if not Path(f"{HOME}/.tmp/{name}").exists():
        os.system(f"wget https://github.com/{href} -O {HOME}/.tmp/{name}")
    ver = re.search("(3.\d{1,}.\d{1,})", name).group(1)
    if not Path(f"{HOME}/.tmp/cpython-{ver}").exists():
        os.system(f"unzip {HOME}/.tmp/{name} -d {HOME}/.tmp/ >/dev/null")

    if not Path(f"{HOME}/.opt/cpython-{ver}").exists():
        os.system(f"cd {HOME}/.tmp/cpython-{ver} && ./configure --prefix {HOME}/.opt/cpython-{ver} >/dev/null && make -j 16 >/dev/null 2>&1 &&  make install >/dev/null")
    else:
        print(f"すでに、cpython-{ver}はインストールされています,再インストールの際は、手動で削除してください")

with ThreadPoolExecutor(max_workers=4) as exe:
    for _ in exe.map(build_and_install, hrefs):
        _

