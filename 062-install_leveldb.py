import os
from pathlib import Path
import shutil
os.system("git clone --recurse-submodules https://github.com/google/leveldb.git")

"""
NOTE: cmakeはshared_libの作成にはデフォルトでは対応しなくなったため、cmakeオプションで有効にする
"""
CMAKE_OPTS = "-DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON -DLEVELDB_BUILD_TESTS=OFF"
os.system(f"cd leveldb && mkdir build && cd build && cmake {CMAKE_OPTS} .. && make -j16")
os.system("cd leveldb/build && sudo make install")
shutil.rmtree("leveldb")
