import os
import platform
from subprocess import Popen
from subprocess import PIPE
import shutil

try:
    os.system('bash -c "pip3 install distro"')
    import distro
except Exception as exc:
    print(exc)
    exit(1)

distro_name, version, codename = distro.linux_distribution(full_distribution_name=False)
print(distro_name)
if distro_name.lower() in {"ubuntu", "debian", "elementary"}:
    """
    if shutil.which("brew") is None:
        # install linux brew
        os.system(
            'bash -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"'
        )
    else:
        os.system("brew upgrade")
    """
    # support apt
    software_list = """
	vim
	tmux
	clang
	gcc
	mosh
	sshfs
	httpie
	pv
	wget
	curl
	zsh
	bat
	progress
	nfs-kernel-server
	hddtemp
	build-essential
	curl
	file
	git
	docker.io
	docker-compose
        rclone
        cargo
        aria2
	"""
    # 先頭の空白を取り除く
    software_list = [s.strip() for s in software_list.split("\n") if s.strip() != ""]

    for software in software_list:
        print(f"sudo apt-get install {software}")
        with Popen(["sudo", "apt-get", "install", software, "-y"], stdout=PIPE, stderr=PIPE, shell=False,) as proc:
            stdout, stderr = proc.communicate()
            print(stdout.decode("utf8", "ignore"))

    software_list = """exa\nbat"""
    software_list = [s.strip() for s in software_list.split("\n") if s.strip() != ""]
    for software in software_list:
        if shutil.which(software) is None:
            # io lock free
            with Popen(["cargo", "install", software]) as proc:
                stdout, stderr = proc.communicate()

    # docker groupに入ってなかったら追加する
    if "docker" not in os.popen("groups $USER").read():
        print("try to put docker group to $USER")
        os.system("sudo usermod -aG docker $USER")
    print("activate docker group.")
    os.system("newgrp docker")
elif distro_name.lower() in {"darwin"}:
    if shutil.which("brew") is not None:
        os.system("brew upgrade")
    else:
        os.system('ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null')

    software_list = """
	vim
	tmux
	clang
	gcc
	mosh
	sshfs
	httpie
	pv
	wget
	curl
	zsh
	bat
	progress
	exa
        aria2
        zzz
        rclone
	"""
    # 先頭の空白を取り除く
    software_list = [s.strip() for s in software_list.split("\n") if s.strip() != ""]

    for software in software_list:
        print(f"brew install {software}")
        with Popen(
            ["brew", "install", software],
            # stdout=PIPE,
            # stderr=PIPE,
            shell=False,
        ) as proc:
            stdout, stderr = proc.communicate()

    if shutil.which("rustc") is None:
        # install rust
        os.system("curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sudo sh")

else:
    print("サポート外のOSです")
